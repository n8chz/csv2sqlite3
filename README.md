# csv2sqlite3

## Quick and dirty CSV importer for SQLITE

Usage

`./csv2sqlite3.sh FILE`

where `FILE` is the name of an existing or to-be-created sqlite3 data file.

Imports each file in the current directory with a name ending in `.csv`, into a table with the same name (but without `.csv`) as the file.

