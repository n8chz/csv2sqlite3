#!/bin/bash

for file_name in $(ls *.csv); do
  table_name="${file_name/.csv/}"
  $(sqlite3 $1 '.mode csv' ".import $file_name $table_name" '.exit')
  echo "supposedly added $table_name to database in $1"
done
